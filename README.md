Wmid - permutations
===================

wmid-permutations is a java program that compares several permuting algorithms.

The program runs each algorithm n times,
after that print the execution time of algorithms and number of fixed points.

Usage
-----

Requires Java version 1.6 or higher.
For the first time, will download all the dependencies, so it may take a little longer.

    gradlew.bat run   # for windows
    ./gradlew run     # for linux

Configuration
-------------

In application.properties file you can set the configuration variables

 - permutation.loop - number of the algorithm calls
 - permutation.sizes - number of items in the list to permute,
   you can check several settings at once separating them using ':'
