package pl.patrykpacewicz.permutation.sort.functions;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class FunctionNLogNTest {
    FunctionNLogN function = new FunctionNLogN();

    @Test
    public void shouldCountValue() {
        assertThat(function.countValue(4)).isEqualTo(5);
    }

    @Test
    public void shouldGetName() {
        assertThat(function.getName()).isEqualTo("nlogn");
    }
}
