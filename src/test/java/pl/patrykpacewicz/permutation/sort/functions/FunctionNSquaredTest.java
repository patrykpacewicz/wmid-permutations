package pl.patrykpacewicz.permutation.sort.functions;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class FunctionNSquaredTest {
    FunctionNSquared function = new FunctionNSquared();

    @Test
    public void shouldCountValue() {
        assertThat(function.countValue(4)).isEqualTo(16);
    }

    @Test
    public void shouldGetName() {
        assertThat(function.getName()).isEqualTo("npow2");
    }}
