package pl.patrykpacewicz.permutation.sort.functions;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class FunctionNCubedTest {
    FunctionNCubed function = new FunctionNCubed();

    @Test
    public void shouldCountValue() {
        assertThat(function.countValue(3)).isEqualTo(27);
    }

    @Test
    public void shouldGetName() {
        assertThat(function.getName()).isEqualTo("npow3");
    }
}
