package pl.patrykpacewicz.permutation.sort.functions;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class FunctionNTest {
    FunctionN function = new FunctionN();

    @Test
    public void shouldCountValue() {
        assertThat(function.countValue(3)).isEqualTo(3);
    }

    @Test
    public void shouldGetName() {
        assertThat(function.getName()).isEqualTo("n");
    }
}
