package pl.patrykpacewicz.permutation;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import pl.patrykpacewicz.permutation.sort.functions.FunctionN;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Task5PermutationTest {
    private Task5Permutation permutation;
    private Random randomMock;

    @Before
    public void setUp() {
        randomMock = mock(Random.class);
        permutation = new Task5Permutation(randomMock);
    }

    @Test
    public void shouldGetName() {
        assertThat(permutation.getName()).isEqualTo("task5");
    }

    @Test
    public void shouldPermutateNumbers() {
        ArrayList<Integer> list = Lists.newArrayList(1, 2, 3, 4, 5);

        when(randomMock.nextInt(anyInt())).thenReturn(2, 2, 1, 4, 3);

        List<Integer> permutationList = permutation.permutation(list);

        assertThat(permutationList).containsExactly(3, 2, 1, 4, 5);
    }
}
