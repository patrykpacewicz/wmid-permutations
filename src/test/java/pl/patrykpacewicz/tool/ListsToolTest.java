package pl.patrykpacewicz.tool;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class ListsToolTest {
    private ListsTool tool = new ListsTool();

    @Test
    public void shouldCreateListOfIntegers() {
        List<Integer> listOfNumbers = tool.createListFillWithSortedNumbers(10);

        assertThat(listOfNumbers).containsExactly(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    }

    @Test
    public void shouldFindStaticPointsInTwoLists() {
        List<Integer> list1 = Lists.newArrayList(1,1,9,1,9,1,1,1,9);
        List<Integer> list2 = Lists.newArrayList(7,7,9,7,9,7,7,7,9);

        int numberOfStaticPonts = tool.getNumberOfStaticPonts(list1, list2);

        assertThat(numberOfStaticPonts).isEqualTo(3);
    }
}
