package pl.patrykpacewicz;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.inject.name.Named;
import pl.patrykpacewicz.permutation.Permutation;
import pl.patrykpacewicz.tool.ListsTool;

import javax.inject.Inject;
import java.io.PrintStream;
import java.util.List;

public class Analysis {
    private final List<Permutation> permutations;
    private final ListsTool listsTool;
    private final PrintStream printStream;
    private final Integer permutationLoop;
    private final Iterable<String> permutationSizes;

    @Inject
    public Analysis(
        List<Permutation> permutations,
        ListsTool listsTool,
        PrintStream printStream,
        @Named("permutation.loop") Integer permutationLoop,
        @Named("permutation.sizes") String permutationSize
    ) {
        this.permutations = permutations;
        this.listsTool = listsTool;
        this.printStream = printStream;
        this.permutationLoop = permutationLoop;
        this.permutationSizes = Splitter.on(':').split(permutationSize);
    }

    public void analyse() {
        printStream.println(logOneLine("name", "size", "point", "count", "time (ms)"));

        for (Permutation permutation: permutations) {
            for (String sSize: permutationSizes) {

                int size = Integer.parseInt(sSize);
                int[] result = new int[size+1];

                List<Integer> list = listsTool.createListFillWithSortedNumbers(size);

                long time = System.currentTimeMillis();
                for (int i = 0; i < permutationLoop; i++) {
                    List<Integer> pList = permutation.permutation(list);
                    result[listsTool.getNumberOfStaticPonts(list, pList)]++;
                }

                time = System.currentTimeMillis() - time;

                printStream.println(logOutput(size, permutation.getName(), time, result));
            }
        }
    }

    private String logOutput(int size, String name, long timeInMs, int[] results) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i< results.length ;i++) {
            if (results[i] > 0) {
                String oneline = logOneLine(
                    name,
                    Integer.toString(size),
                    Integer.toString(i),
                    Integer.toString(results[i]),
                    Long.toString(timeInMs)
                );
                stringBuilder.append(oneline);
            }
        }

        return stringBuilder.toString();
    }

    private String logOneLine(String... elements) {
        return Joiner.on("; ").join(elements) + "\r\n";
    }
}
