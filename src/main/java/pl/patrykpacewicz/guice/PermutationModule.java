package pl.patrykpacewicz.guice;

import com.google.common.collect.Lists;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.name.Names;
import pl.patrykpacewicz.permutation.*;
import pl.patrykpacewicz.permutation.sort.functions.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Properties;

public class PermutationModule extends AbstractModule {
    @Override
    protected void configure() {
        Names.bindProperties(binder(), loadProperties("application.properties"));
        bind(PrintStream.class).toInstance(System.out);
    }

    @Provides
    protected List<Permutation> getPermutations(Injector injector) {
        List<Permutation> permutations = Lists.newArrayList();
        permutations.add(getSortPermutation(injector, new FunctionN()));
        permutations.add(getSortPermutation(injector, new FunctionNSquared()));
        permutations.add(getSortPermutation(injector, new FunctionNCubed()));
        permutations.add(getSortPermutation(injector, new FunctionNLogN()));
        permutations.add(injector.getInstance(InPlacePermutation.class));
        permutations.add(injector.getInstance(CyclicPermutation.class));
        permutations.add(injector.getInstance(Task5Permutation.class));

        return permutations;
    }

    private Permutation getSortPermutation(Injector injector, Function function) {
        SortPermutation permutation = injector.getInstance(SortPermutation.class);
        permutation.setFunction(function);
        return permutation;
    }

    private Properties loadProperties(String filename) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

}
