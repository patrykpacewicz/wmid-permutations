package pl.patrykpacewicz.tool;

import com.google.common.collect.Lists;
import java.util.List;

public class ListsTool {
    public List<Integer> createListFillWithSortedNumbers(int size) {
        List<Integer> list = Lists.newArrayList();
        for (int i = 0; i < size; i++) {
            list.add(i);
        }

        return list;
    }

    public int getNumberOfStaticPonts(List<?> list, List<?> list2) {
        int staticpoints = 0;
        for (int i = 0; i< list.size(); i++) {
            if (list.get(i).equals(list2.get(i)) ) {
                staticpoints++;
            }
        }

        return staticpoints;
    }
}
