package pl.patrykpacewicz;

import com.google.inject.Guice;
import pl.patrykpacewicz.guice.PermutationModule;

import javax.inject.Inject;

public class Application {
    private final Analysis analysis;

    @Inject
    public Application(Analysis analysis) {
        this.analysis = analysis;
    }

    public static void main(String[] args) {
        Guice.createInjector(new PermutationModule()).getInstance(Application.class).run();
    }

    public void run() {
        try {
            analysis.analyse();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
