package pl.patrykpacewicz.permutation.sort.functions;

public class FunctionNCubed implements Function {
    @Override
    public int countValue(int n) {
        return (int) Math.pow(n, 3);
    }

    @Override
    public String getName() {
        return "npow3";
    }
}
