package pl.patrykpacewicz.permutation.sort.functions;

public class FunctionNSquared implements Function {
    @Override
    public int countValue(int n) {
        return (int) Math.pow(n, 2);
    }

    @Override
    public String getName() {
        return "npow2";
    }
}
