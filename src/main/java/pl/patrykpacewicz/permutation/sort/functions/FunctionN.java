package pl.patrykpacewicz.permutation.sort.functions;

public class FunctionN implements Function {
    @Override
    public int countValue(int n) {
        return n;
    }

    @Override
    public String getName() {
        return "n";
    }
}
