package pl.patrykpacewicz.permutation.sort.functions;

public interface Function {
    public int countValue(int n);
    public String getName();
}
