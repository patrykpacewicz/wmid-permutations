package pl.patrykpacewicz.permutation.sort.functions;

public class FunctionNLogN implements Function {
    @Override
    public int countValue(int n) {
        return (int) (n * Math.log(n));
    }

    @Override
    public String getName() {
        return "nlogn";
    }
}
