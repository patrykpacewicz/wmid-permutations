package pl.patrykpacewicz.permutation;

import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.List;
import java.util.Random;

public class InPlacePermutation implements Permutation {
    private final Random random;

    @Inject
    public InPlacePermutation(Random random) {
        this.random = random;
    }

    @Override
    public List<Integer> permutation(List<Integer> list) {
        // A[i] <-> A[Random(i, n)]
        List<Integer> result = Lists.newArrayList(list);

        for (int i = 0; i< list.size(); i++) {
            int randIndex = random.nextInt(list.size() - i) + i;
            Integer tmpValue = result.get(randIndex);
            result.set(randIndex, result.get(i));
            result.set(i, tmpValue);
        }

        return result;
    }

    @Override
    public String getName() {
        return "in-place";
    }
}
