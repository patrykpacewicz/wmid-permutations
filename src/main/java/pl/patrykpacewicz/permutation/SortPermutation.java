package pl.patrykpacewicz.permutation;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import pl.patrykpacewicz.permutation.sort.functions.Function;

import javax.inject.Inject;
import java.util.*;

public class SortPermutation implements Permutation {
    private Random random;
    private Function function;

    @Inject
    public SortPermutation(Random random) {
        this.random = random;
    }

    public String getName() {
        return "sort:" + function.getName();
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public List<Integer> permutation(List<Integer> list) {
        List<Integer> tmpList             = Lists.newArrayList(list);
        List<Integer> permutationSortList = getPermutationSortList(list.size());

        return sortListUsingAnotherList(tmpList, permutationSortList);
    }

    private List<Integer> getPermutationSortList(int size) {
        List<Integer> permutationSortList = Lists.newArrayList();
        int permutationMaxValue = function.countValue(size);
        int i = random.nextInt(permutationMaxValue);

        while (permutationSortList.size() != size) {
            permutationSortList.add(i);
            i = random.nextInt(permutationMaxValue);
        }

        return permutationSortList;
    }

    private List<Integer> sortListUsingAnotherList(final List<Integer> list, final List<Integer> permutationSortList) {
        Map<Integer,Integer> count    = Maps.newHashMap();
        Integer[]            output   = new Integer[list.size()];
        int                  total    = 0;
        int                  oldCount = 0;

        for (int i = 0; i < permutationSortList.size(); i++){
            if (!count.containsKey(permutationSortList.get(i))) {
                count.put(permutationSortList.get(i),0);
            }
            count.put(permutationSortList.get(i),count.get(permutationSortList.get(i)) + 1);
        }

        for (Integer i : count.keySet()) {
            oldCount = count.get(i);
            count.put(i, total);
            total += oldCount;
        }

        for (int i = 0; i < permutationSortList.size(); i++){
            output[count.get(permutationSortList.get(i))] = list.get(i);
            count.put(permutationSortList.get(i),count.get(permutationSortList.get(i)) + 1);
        }

        return Lists.newArrayList(output);
    }
}
