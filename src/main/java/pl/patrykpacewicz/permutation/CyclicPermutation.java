package pl.patrykpacewicz.permutation;

import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.List;
import java.util.Random;

public class CyclicPermutation implements Permutation {
    private final Random random;

    @Inject
    public CyclicPermutation(Random random) {
        this.random = random;
    }

    @Override
    public List<Integer> permutation(List<Integer> list) {
        int l = random.nextInt(list.size());
        List<Integer> result = Lists.newArrayList();

        for (int i = 0; i < list.size(); i++) {
            int k = (i + l) % list.size();
            result.add(list.get(k));
        }

        return result;
    }

    @Override
    public String getName() {
        return "cyclic";
    }
}
