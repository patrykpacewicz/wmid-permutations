package pl.patrykpacewicz.permutation;

import java.util.List;

public interface Permutation {
    public List<Integer> permutation(List<Integer> list);
    public String getName();
}
